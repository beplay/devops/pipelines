# Devops pipeline documents

## Upgrade tools

```sh
CURRENT=registry.gitlab.com/beplay/devops/tools/cicd-toolchain:1.0.0
NEW=registry.gitlab.com/beplay/devops/tools/cicd-toolchain:1.0.0

find . -type f -name "*.yml" -exec  sed -i '' -e "s|$CURRENT|$NEW|g" {} + 2> /dev/null

```
